//Task 1 Java
/*public class Ayan {
    public static int findMinRec(int A[], int n)
    {
        int min = A[0];
        for(int i=1;i<A.length;i++){
            if(A[i] < min){
                min = A[i];
            }
        }
        return min;
    }
    public static void main(String args[])
    {
        int A[] = {10, 1, 32, 3, 45};
        int n = A.length;
        System.out.println(findMinRec(A, n));
    }
}
*/
//Task 2 Java

/*
public class Ayan {

    public static double average(int a[], int n,int i,int sum)
    {
        if(i==n){
            return sum;
        }
        sum=sum+a[i];

        return average(a,n,i+1,sum);

    }

    public static void main (String[] args)
    {
        int arr[] = {3, 2, 4, 1};
        int n = arr.length;
        int i=0; double f;
        int sum=0;
        f=(average(arr, n,i,sum))/n;
        System.out.println(f);
    }
}
 */
//Task 3 Java

/*
import java.util.Scanner;

public class Ayan {
    static boolean isPrime(int n)
    {
        if (n <= 1)
            return false;

        for (int i = 2; i < n; i++)
            if (n % i == 0)
                return false;

        return true;
    }

    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);

        int num = scan.nextInt();

        if (isPrime(num))
            System.out.println(" Prime");
        else
            System.out.println(" Composite");

    }
}
 */
//Task 4 Java

/*
import java.util.Scanner;
public class Ayan {
    static int factorial(int n)
    {
        if (n == 0)
            return 1;
        return n*factorial(n-1);
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println( factorial(5));
    }
}
 */
//Task 5 Java

/*
public class Ayan {
    public static void main(String args[]) {
        int number=17 ;
            System.out.print(fibonacci2(number) +" ");
    }
    public static int fibonacci(int number){
        if(number == 1 || number == 2){
            return 1;
        }
        return fibonacci(number-1) + fibonacci(number -2);
    }
    public static int fibonacci2(int number){
        if(number == 1 || number == 2){
            return 1;
        }
        int fibo1=1, fibo2=1, fibonacci=1;
        for(int i= 3; i<= number; i++){
            fibonacci = fibo1 + fibo2;
            fibo1 = fibo2;
            fibo2 = fibonacci;
        }
        return fibonacci;
    }
}
 */
//Task 6 Java

/*
import java.util.Scanner;
public class Ayan {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        System.out.print(function(a,b) +" ");
    }
    public static int function(int a,int b){
        if(b == 0){
            return 1;
        }
        else
            return (a*function(a,b-1));
    }
}
 */
//Task 7 Java

/*
public class Ayan {
    static void reverse(int arr[], int start, int end)
    {
        int temp;
        if (start >= end)
            return;
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        reverse(arr, start+1, end-1);
    }
    static void printArray(int arr[], int size)
    {
        for (int i=0; i < size; i++)
            System.out.print(arr[i] + " ");
    }
    public static void main (String[] args) {
        int arr[] = {1, 4, 6, 2};
        reverse(arr, 0, 3);
        printArray(arr, 4);
    }
}
 */
//Task 8 Java

/*
import java.util.Scanner;
public class Ayan {
    static boolean isNumber(String s)
    {
        for (int i = 0; i < s.length(); i++)
            if (Character.isDigit(s.charAt(i)) == false)
                return false;
        return true;
    }
    static public void main (String[] args)
    {
        Scanner myObj = new Scanner(System.in);
        String str;
        str = myObj.nextLine();
        if (isNumber(str))
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
 */
//Task 9 Java

/*
import java.util.Scanner;
public class Ayan {
    static int binom(int n, int k)
    {
        if (k == 0 || k == n)
            return 1;
        return binom(n - 1, k - 1) +
                binom(n - 1, k);
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();
        System.out.println(binom(n, k));
    }
}
 */
//Task 10 Java

/*
import java.util.Scanner;
public class Ayan {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n1 = scan.nextInt();
        int n2 = scan.nextInt();
        System.out.println(s(n1, n2));
    }
    public static int s(int n1, int n2)
    {
        if (n2 != 0)
            return s(n2, n1 % n2);
        else
            return n1;
    }
}
 */
//Task b1 Java

/*
public class Ayan {
    static int count(int arr[], int n)
    {
        int c = 1;
        for (int i = 1; i < n; i++)
        {
            int j = 0;
            for (j = 0; j < i; j++)
                if (arr[i] == arr[j])
                    break;
            if (i == j)
                c++;
        }
        return c;
    }
    public static void main (String[] args)

    {
        int arr[] = {1, 8, 1, 8, 8, 10};
        int n = arr.length;
        System.out.println(count(arr, n));
    }
}
 */
//Task b2 Java

/*
import java.util.Scanner;
public class Ayan {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for (int i = 1; i <= n; i++) {
            if (n == i) {
                for (int j = 0; j < n; j++) {
                    System.out.print(n / i + " ");
                }
                break;
            }
            System.out.println(n - i + " " + i);
        }
    }
}
 */
//Task b3 Java

/*
import java.util.Scanner;
public class Ayan {
    public void solve(int n, String start, String auxiliary, String end) {
        if (n == 1) {
            System.out.println(start + " -> " + end);
        } else {
            solve(n - 1, start, end, auxiliary);
            System.out.println(start + " -> " + end);
            solve(n - 1, auxiliary, start, end);
        }
    }
    public static void main(String[] args) {
        Ayan a = new Ayan();
        Scanner scanner = new Scanner(System.in);
        int discs = scanner.nextInt();
        a.solve(discs, "A", "B", "C");
    }
}
 */